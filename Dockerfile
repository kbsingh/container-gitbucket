
FROM registry.centos.org/centos/centos:7

MAINTAINER Karanbir Singh <container-feedback.inb@karan.org>

ENV APPVER=4.15.0

LABEL License=GPLv2
LABEL Version=${APPVER}

COPY ./root /
RUN yum -y install --setopt=tsflags=nodocs git java-1.8.0-openjdk && \
    yum -y upgrade && yum clean all && \
    curl -L -o /opt/git/gitbucket.war https://github.com/gitbucket/gitbucket/releases/download/${APPVER}/gitbucket.war && \
    chmod 777 /srv/git 

EXPOSE 3000

USER 1000

ENTRYPOINT ["/usr/local/bin/run.sh" ]
